/**
 * @file
 * Drupal Charts plugin.
 *
 * This alters the existing CKEditor table and adds the chart functionality.
 *
 * @see \Drupal\ckeditor_chart\Plugin\CKEditorPlugin\Chart
 *
 * @ignore
 */

(function ($, Drupal, drupalSettings, CKEDITOR) {

  'use strict';
  var commandName = 'insertChart';
  CKEDITOR.plugins.add('chart', {
    init: function(editor) {
      editor.addCommand(commandName, new CKEDITOR.dialogCommand(commandName));
      editor.ui.addButton('ChartsButton', {
        label: 'Charts',
        command: commandName,
        icon: this.path + 'icons/charts.png',
      });
      editor.on('doubleclick', function (event) {
        // Ensure that the table has the table row set before enabling the
        // ability to double click and bring up the chart settings.
        var selector = event.editor.getSelection().getStartElement();
        var table = selector.getAscendant('table');
        if (table) {
          var totalHeaderCount = table.find('th').count();
          if (totalHeaderCount > 0) {
            event.data.dialog = commandName;
          }
        }
      });
      editor.on('elementsPathUpdate', function (event) {
        var selector = event.editor.getSelection().getStartElement();
        var table = selector.getAscendant('table');
        if (table && table.hasClass('use-charts')) {
          event.editor.commands[commandName].setState(CKEDITOR.TRISTATE_ON);
        }
        else {
          event.editor.commands[commandName].setState(CKEDITOR.TRISTATE_OFF);
        }
      });
    }
  });

  CKEDITOR.dialog.add('insertChart', function (editor) {
    var positionSelection = [
      ['Vertical', 'vertical'],
      ['Horizontal', 'horizontal'],
    ];

    // Get the editor config by using editor.config.chart_colors and
    var colors = editor.config.chart_colors;

    colors = colors.split("\n");
    var colorSelector = [];
    var colorDom  = '';
    for (var i = 0; i < colors.length; i++) {
      // Remove all line breaks.
      var color = colors[i].replace(/\r?\n|\r/gm, '').toLowerCase();
      colorSelector.push([color, color]);
      // Build a fake color picker that just restricts to the colors in
      // settings.
      colorDom += '<div class="color-box" style="text-indent:-9999px;background-color:' + color + ';width:25px;height:25px;">' + color + '</div>';
    }
    colorDom = '<div class="color-palette-wrapper" style="display:flex">' + colorDom + '</div>';
    var graphType = [['None', '']];
    var graphOptions = editor.config.chart_graphType.split("\n");
    for (var i = 0; i < graphOptions.length; i++) {
      var graphName = graphOptions[i].replace(/\r?\n|\r/gm, '');
      graphType.push([graphName.charAt(0).toUpperCase() + graphName.substr(1), graphName]);
    }

    var selector = editor.getSelection().getStartElement();
    var table = selector.getAscendant('table');

    var totalItemCount = table.find('tr').count();
    var childrenFields = [];
    for (var i = 1; i < totalItemCount; i++) {
      childrenFields.push(i)
    }

    var contents = {
      title: 'Chart Settings',
      contents: [
        {
          id: 'chart-settings',
          label: 'Settings',
          // Build the elements within the dialog box.
          elements: [
            {
              type: 'select',
              id: 'graph_type',
              label: 'Graph Type',
              items: graphType,
              setup: function (element) {
                var value = element.getAttribute('data-graph-type');
                if (!value) {
                  value = '';
                }
                this.setValue(value);
              },
              commit: function (data) {
                data.graph_type = this.getValue();
              }
            },
            {
              type: 'select',
              id: 'position',
              label: 'Legend Position',
              items: positionSelection,
              setup: function (element) {
                var value = element.getAttribute('data-graph-legend-layout');
                if (!value) {
                  value = 'vertical';
                }
                this.setValue(value);
              },
              commit: function (data) {
                data.position = this.getValue();
              }
            },
            {
              type: 'vbox',
              id: 'columnColor',
              align: 'left',
              width: '300px',
              children: childrenFields.map(function (i) {

                return {
                  id: 'row--' + i,
                  label: 'Colors',
                  type: 'select',
                  items: colorSelector,
                  onChange: function (api) {
                    var $currentSelect = $('select', '#' + this.domId);
                    $currentSelect.css('border-left', '20px solid ' + this.getValue());
                    if ($currentSelect.parent('div').find('.color-palette-wrapper').length === 0) {
                      $currentSelect.parent('div').prepend(colorDom);
                      $currentSelect.parent('div').find('.color-box').bind('click', function () {
                        $currentSelect.val($(this).text());
                        $currentSelect.css('border-left', '20px solid ' + $(this).text());
                      })
                    }
                  },
                  commit: function (data) {
                    data.columnColor.push(this.getValue());
                  },
                  setup: function (element) {
                    var value = element.getAttribute('data-graph-color-' + i);
                    var tdDom = table
                      .find('tr')
                      .getItem(i)
                      .find('td')
                      .getItem(0);
                    this.setLabel('Color for: row ' + tdDom.getText());
                    if (!value) {
                      value = '';
                    }
                    this.setValue(value);
                  },
                }
              }),
              commit: function (data) {
                data.columnColor = [];
              }
            },
          ]
        }
      ],
      /**
       * Callback that fires on every time the dialog is shown.
       *
       * Returns the element to the content template in the setup function.
       */
      onShow: function () {
        var selector = editor.getSelection().getStartElement();
        var table = selector.getAscendant('table');
        if (table) {
          this.setupContent(table);
        }
      },
      /**
       * Callback when the hitting ok/save on the dialog box.
       */
      onOk: function () {
        var data = [];
        var selector = editor.getSelection().getStartElement();
        var table = selector.getAscendant('table');
        this.commitContent(data);
        if (table) {
          // Basic settings in the chart configuration added to the table
          // header.  These does not need to be remove when the charts is turned
          // off.
          table.setAttribute('data-graph-legend-layout', data.position)
          table.setAttribute('data-graph-type', data.graph_type);
          table.setAttribute('data-graph-pie-show-in-legend', 1);

          data.columnColor.forEach(function (value, index) {
            table.setAttribute('data-graph-color-' + (index + 1), value);
          });
          if (data.graph_type !== '') {
            // Add the required attribute and classes to make highchart render.
            table.setAttribute('data-graph-container-before', 1);
            table
              .addClass('use-charts')
              .addClass('visually-hidden');
          }
          else {
            // Turn off charts.
            table
            .removeClass('use-charts')
            .removeClass('visually-hidden');
          }
        }
      }
    };

    return contents;
  });

})(jQuery, Drupal, drupalSettings, CKEDITOR);

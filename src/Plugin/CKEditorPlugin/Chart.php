<?php

namespace Drupal\ckeditor_simple_highcharts\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "CKEditor Charts" plugin.
 *
 * @CKEditorPlugin(
 *   id = "chart",
 *   label = @Translation("CKEditor Chart")
 * )
 */
class Chart extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {

  /**
   * Get the library of where the plugin resides.
   */
  public function getLibraryPath() {
    return drupal_get_path('module', 'ckeditor_simple_highcharts') . '/js/plugins/' . $this->getPluginId();
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getLibraryPath() . '/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(Editor $editor) {
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
    $settings = $editor->getSettings();

    $form['colors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Colors'),
      '#description' => $this->t('Enter the hex values of all colors separated by a line break'),
      '#default_value' => !empty($settings['plugins']['chart']['colors']) ? $settings['plugins']['chart']['colors'] : '',
    ];
    $form['graph_type'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Graph Types'),
      '#description' => $this->t('Enter the hex values of all graph type separated by a line break'),
      '#default_value' => !empty($settings['plugins']['chart']['graph_type']) ? $settings['plugins']['chart']['graph_type'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $path = $this->getLibraryPath();
    return [
      'ChartsButton' => [
        'label' => $this->t('Charts'),
        'image' => $path . '/icons/charts.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $settings = $editor->getSettings();
    $config = [];

    if (!empty($settings['plugins']['chart']['colors'])) {
      $config['chart_colors'] = $settings['plugins']['chart']['colors'];
    }

    if (!empty($settings['plugins']['chart']['graph_type'])) {
      $config['chart_graphType'] = $settings['plugins']['chart']['graph_type'];
    }

    return $config;
  }

}
